/**
 * Have the function ScaleBalancing(strArr) read strArr which will contain two elements,
 * the first being the two positive integer weights on a balance scale (left and right sides)
 * and the second element being a list of available weights as positive integers.
 * Your goal is to determine if you can balance the scale by using the least amount of weights from the list,
 * but using at most only 2 weights.
 * For example: if strArr is ["[5, 9]", "[1, 2, 6, 7]"] then this means there is a balance scale
 * with a weight of 5 on the left side and 9 on the right side.
 * It is in fact possible to balance this scale by adding a 6 to the left side from the list of weights
 * and adding a 2 to the right side.
 * Both scales will now equal 11 and they are perfectly balanced.
 * Your program should return a comma separated string of the weights that were used from the list
 * in ascending order, so for this example your program should return the string 2,6 
 * There will only ever be one unique solution and the list of available weights will not be empty.
 * It is also possible to add two weights to only one side of the scale to balance it.
 * If it is not possible to balance the scale then your program should return the string not possible.
 */

function getWeights(arr) {
    let res = [];
    for (let i = 1; i < arr.length - 1; i++) {
        res.push(Number(arr[i]));
    }
    return res;
}

function sortWeights(arr) {
    for (let i = 0; i < arr.length; i++) {
        for (let j = i + 1; j < arr.length; j++) {
            if (i !== j && arr[j] > arr[i]) {

                // switch
                let tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }
    }
    return arr;
}

function sortAnswer(arr) {
    for (let i = 0; i < arr.length; i++) {
        for (let j = i + 1; j < arr.length; j++) {
            if (i !== j && arr[j] < arr[i]) {

                // switch
                let tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }
    }
    return arr;
}

function correctAnswer(arr) {
    let res;
    let first = true;
    for (let i = 0; i < arr.length; i++) {
        if (first) {
            res = arr[i].toString();
            first = false;
        }
        else
            res += "," + arr[i].toString();
    }
    return res;
}

function ScaleBalancing(strArr) {

    // initialize
    let regex = /[\[,\]]/;

    let tmp = strArr[0].split(regex);
    let left = Number(tmp[1]);
    let right = Number(tmp[2]);

    tmp = strArr[1].split(regex);
    let weights = getWeights(tmp);

    // sort weights
    weights = sortWeights(weights);

    // start algorithm
    let ans = [];

    let index = 1;

    while (left !== right && index !== weights.length) {

        if (left < right) {

            if ((left + weights[index]) <= right) {
                left += weights[index];
                ans.push(weights[index]);
            }

        }
        // right < left
        else {

            if ((right + weights[index]) <= left) {
                right += weights[index];
                ans.push(weights[index]);
            }
        }
        index++;
    }
    //ans = sortAnswer(ans);
    ans = sortAnswer(ans);
    let res = correctAnswer(ans);
    return res;
}

console.log(ScaleBalancing(["[3, 4]", "[1, 2, 7, 7]"]));
// 1

console.log(ScaleBalancing(["[13, 4]", "[1, 2, 3, 6, 14]"]));
// 3,6