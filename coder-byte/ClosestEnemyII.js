/**
 * Have the function ClosestEnemyII(strArr) read the matrix of numbers stored in strArr
 * which will be a 2D matrix that contains only the integers 1, 0, or 2.
 * Then from the position in the matrix where a 1 is,
 * return the number of spaces either left, right, down, or up
 * you must move to reach an enemy which is represented by a 2.
 * You are able to wrap around one side of the matrix to the other as well.
 * For example: if strArr is ["0000", "1000", "0002", "0002"] then this looks like the following:
 * 0 0 0 0
 * 1 0 0 0
 * 0 0 0 2
 * 0 0 0 2
 * For this input your program should return 2 because the closest enemy (2)
 * is 2 spaces away from the 1 by moving left to wrap to the other side and then moving down once.
 * The array will contain any number of 0's and 2's, but only a single 1.
 * It may not contain any 2's at all as well, where in that case your program should return a 0.
 */

function ClosestEnemyII(strArr) {

    const size = strArr.length;

    // initialize
    let matrix = new Array(size);
    for (let i = 0; i < size; i++) {
        matrix[i] = new Array(size);
    }

    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            matrix[i][j] = strArr[i].charAt(j);
        }
    }

    let x, y
    let two = [];

    // start game
    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {

            // start algorithm
            if (matrix[i][j] === "1") {

                // save '1' location
                x = i;
                y = j;
            }
            else if (matrix[i][j] === "2") {

                // save '2' location
                two.push([i, j]);
            }
        }
    }

    let smaller;
    for (let i = 0; i < two.length; i++) {

        let tmp = Math.abs(two[i][0] - x) % (size - 1) + Math.abs(two[i][1] - y) % (size - 1);
        if (i === 0) {
            smaller = tmp;
        }
        else if (tmp < smaller) {
            smaller = tmp;
        }
    }

    if (smaller === 0) {
        return smaller + 1;
    }
    return smaller;
}

console.log(ClosestEnemyII(["000", "100", "200"]));
// 1

console.log(ClosestEnemyII(["0000", "2010", "0000", "2002"]));
// 2