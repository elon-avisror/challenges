/**
 * Have the function HTMLElements(str) read the str parameter being passed
 * which will be a string of HTML DOM elements and plain text.
 * The elements that will be used are: b, i, em, div, p.
 * For example: if str is "<div><b><p>hello world</p></b></div>" then this string of DOM elements
 * is nested correctly so your program should return the string true.
 * If a string is not nested correctly, return the first element encountered where,
 * if changed into a different element, would result in a properly formatted string.
 * If the string is not formatted properly, then it will only be one element that needs to be changed.
 * For example: if str is "<div><i>hello</i>world</b>" then your program should return the string div
 * because if the first <div> element were changed into a <b>, the string would be properly formatted. 
 */

function HTMLElements(str) {

    let ans = "";
    for (let i = 0; i < str.length; i++) {
        if (str[i] === "<") {
            ans += str[i];
            while (str[i] !== ">") {
                i++;
                ans += str[i];
            }
        }
        else {
            ans += "!";
        }
    }

    ans = ans.split(/<|>/gm);
    
    let tmp;
    let stack = [];

    for (let i = 0; i < ans.length; i++) {

        // b, i, em, div, p
        switch (ans[i]) {
            case "b":
                stack.push("b");
                break;
            case "/b":
                tmp = stack.pop();
                if (tmp !== "b") {
                    return tmp;
                }
                break;
            case "i":
                stack.push("i");
                break;
            case "/i":
                tmp = stack.pop();
                if (tmp !== "i") {
                    return tmp;
                }
                break;
            case "em":
                stack.push("em");
                break;
            case "/em":
                tmp = stack.pop();
                if (tmp !== "em") {
                    return tmp;
                }
                break;
            case "div":
                stack.push("div");
                break;
            case "/div":
                tmp = stack.pop();
                if (tmp !== "div") {
                    return tmp;
                }
                break;
            case "p":
                stack.push("p");
                break;
            case "/p":
                tmp = stack.pop();
                if (tmp !== "p") {
                    return tmp;
                }
                break;
            default:
                break;
        }
    }

    // is empty ?
    if (stack.length === 0) {
        return true;
    }
    return stack.pop();
}

console.log(HTMLElements("<div><div><b></b></div></p>"));
// div

console.log(HTMLElements("<div>abc</div><p><em><i>test test test</b></em></p>"));
// i

console.log(HTMLElements("<div><b><p>hello world</p></b></div>"));
// div

console.log(HTMLElements("<div>"));
// div