/**
 * Have the function LetterCapitalize(str) take the str parameter being passed
 * and capitalize the first letter of each word.
 * Words will be separated by only one space. 
 */

function LetterCapitalize(str) {

    str = str.split(" ");
    for (let i = 0; i < str.length; i++) {
        str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
    }
    return str.join(" ");
}

console.log(LetterCapitalize("hello world"));
// Hello World

console.log(LetterCapitalize("i ran there"));
// I Ran There