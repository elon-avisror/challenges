/**
 * Have the function LetterChanges(str) take the str parameter being passed
 * and modify it using the following algorithm.
 * Replace every letter in the string with the letter following it in the alphabet
 * (ie. c becomes d, z becomes a).
 * Then capitalize every vowel in this new string (a, e, i, o, u) and finally return this modified string. 
 */

function LetterChanges(str) {

    let res = str.replace(/[a-z]/gi, function (c) {
        switch (c) {
            case 'z': return 'a';
            case 'Z': return 'A';
            default: return String.fromCharCode(1 + c.charCodeAt(0));
        }
    });
    return res.replace(/[aeiou]+/g, function (vowel) {
        return vowel.toUpperCase();
    });
}

console.log(LetterChanges("hello*3"));
// Ifmmp*3

console.log(LetterChanges("fun times!"));
// gvO Ujnft!