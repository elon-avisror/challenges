/**
 * Have the function ArrayAddition(arr) take the array of numbers stored in arr
 * and return the string true if any combination of numbers in the array
 * (excluding the largest number) can be added up to equal the largest number in the array,
 * otherwise return the string false.
 * For example: if arr contains [4, 6, 23, 10, 1, 3] the output should return true
 * because 4 + 6 + 10 + 3 = 23. The array will not be empty,
 * will not contain all the same elements, and may contain negative numbers.
 */

function allAddCombinations(arr, biggest) {

    let result = [];
    let power = Math.pow;
    let size = power(2, arr.length);

    // Time & Space Complexity O (n * 2^n)

    for (let i = 0; i < size; i++) {
        let temp = 0;

        for (let j = 0; j < arr.length; j++) {

            // & is bitwise AND
            if (arr[j] !== biggest && (i & power(2, j))) {
                temp += arr[j];
            }
        }
        result.push(temp);
    }
    return result;
}

function ArrayAddition(arr) {

    // initialization
    let biggest = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > biggest) {
            // getting the biggest number in the array
            biggest = arr[i];
        }
    }

    let combinations = allAddCombinations(arr, biggest);
    for (let i = 0; i < combinations.length; i++) {
        if (combinations[i] === biggest) {
            return true;
        }
    }

    return false;
}

console.log(ArrayAddition([1, 2, 3, 6]));
// true

console.log(ArrayAddition([5, 7, 16, 1, 2]));
// "false"

console.log(ArrayAddition([3, 5, -1, 8, 12]));
// "true"

console.log(ArrayAddition([12, 2, 8, 12]));
// "false"

console.log(ArrayAddition([4, 6, 23, 10, 1, 3]));
// "true"

console.log(ArrayAddition([1, 1, 1, 1, 6]));
// "false"