/**
 * Have the function SimpleSymbols(str) take the str parameter being passed
 * and determine if it is an acceptable sequence by either returning the string true or false.
 * The str parameter will be composed of + and = symbols with several characters between them
 * (ie. ++d+===+c++==a) and for the string to be true each letter must be surrounded by a + symbol.
 * So the string to the left would be false.
 * The string will not be empty and will have at least one letter.
 */

function SimpleSymbols(str) {

    let flag = false;
    str = str.split(/[=0-9]/);
    for (let i = 0; i < str.length; i++) {
        if (str[i] === "")
            continue;
        else if (str[i].charAt(0) === "+" && str[i].charAt(1).match(/[a-z]/) && str[i].charAt(2) === "+")
            flag = true;
        else
            return false;
    }
    return flag;
}

console.log(SimpleSymbols("+d+=3=+s+"));
// true

console.log(SimpleSymbols("f++d+"));
// false