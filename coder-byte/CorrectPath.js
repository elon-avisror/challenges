/**
 * Have the function CorrectPath(str) read the str parameter being passed,
 * which will represent the movements made in a 5x5 grid of cells starting from the top left position.
 * The characters in the input string will be entirely composed of: r, l, u, d, ?.
 * Each of the characters stand for the direction to take within the grid,
 * for example: r = right, l = left, u = up, d = down.
 * Your goal is to determine what characters the question marks should be
 * in order for a path to be created to go from the top left of the grid
 * all the way to the bottom right without touching previously travelled on cells in the grid. 
 * For example: if str is "r?d?drdd" then your program should output the final correct string
 * that will allow a path to be formed from the top left of a 5x5 grid to the bottom right.
 * For this input, your program should therefore return the string rrdrdrdd.
 * There will only ever be one correct path and there will always be at least one question mark
 * within the input string. 
 */

function CorrectPath(str) {

    const limit = 0;
    const steps = 4;

    let q_num = 0;
    let r_num = 0;
    let l_num = 0;
    let d_num = 0;
    let u_num = 0;

    str = str.split("");

    // for counting marks
    for (let i = 0; i < str.length; i++) {

        switch (str[i]) {
            case "?":
                q_num++;
                break;

            case "r":
                r_num++;
                break;

            case "l":
                l_num++;
                break;

            case "d":
                d_num++;
                break;

            case "u":
                u_num++;
                break;

            default:
                return "input error!";
        }
    }

    let d_steps;
    let u_steps;
    let r_steps;
    let l_steps;

    let t_downs = d_num - u_num;
    let t_rights = r_num - l_num;

    r_num = 0;
    l_num = 0;
    d_num = 0;
    u_num = 0;

    // start algorithm
    for (let i = 0; i < str.length; i++) {

        switch (str[i]) {
            case "?":

                let a_downs = d_num - u_num;
                let a_rights = r_num - l_num;

                const topLeft = (a_downs >= limit && a_downs < steps);
                const buttomRight = (a_rights >= limit && a_rights < steps);

                if (topLeft) {

                    // do down
                    str[i] = "d";
                    d_num++;
                    t_downs--;
                }
                else if (buttomRight) {

                    // do right
                    str[i] = "r";
                    r_num++;
                    t_rights--;
                }

                // do oposite
                else {

                    if ((str[i - 1] === "r" || str[i - 1] === "l" || str[i - 1] === "u") && a_downs > limit && a_downs < steps) {
                        str[i] = "u";
                        u_num++;
                        t_downs--;
                    }
                    else if ((str[i - 1] !== "u" || str[i - 1] === "d" || str[i - 1] === "l") && a_rights > limit && a_rights < steps) {
                        str[i] = "l";
                        l_num++;
                        t_rights--;
                    }
                }
                break;

            case "r":
                r_num++;
                break;

            case "l":
                l_num++;
                break;

            case "d":
                d_num++;
                break;

            case "u":
                u_num++;
                break;

            default:
                return "input error!";
        }
    }

    return str.join("");
}

console.log(CorrectPath("???rrurdr?"));
// dddrrurdrd

console.log(CorrectPath("drdr??rrddd?"));
// drdruurrdddd