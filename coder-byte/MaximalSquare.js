/**
 * Have the function MaximalSquare(strArr) take the strArr parameter being passed
 * which will be a 2D matrix of 0 and 1's, and determine the area of the largest square submatrix that contains all 1's.
 * A square submatrix is one of equal width and height,
 * and your program should return the area of the largest submatrix that contains only 1's.
 * For example: if strArr is ["10100", "10111", "11111", "10010"] then this looks like the following matrix:
 * 1 0 1 0 0
 * 1 0 1 1 1
 * 1 1 1 1 1
 * 1 0 0 1 0
 * For the input above, you can see the bolded 1's create the largest square submatrix of size 2x2,
 * so your program should return the area which is 4.
 * You can assume the input will not be empty.
 */

function FindSubMetrix(submetrix, i, j) {

    let cnt = 1;
    // start counting matrix from i,j to n,n and with n-i+1 === n-j+1
    for (let m = i; m < submetrix.length; m++) {
        for (let k = j; k < submetrix[m].length; k++) {
            if (submetrix[m][k] === "1") {
                if (m - i + 1 === k - j + 1 && m !== i && k !== j && (m - i + 1) * (k - j + 1) === cnt) {
                    return cnt;
                }
                cnt++;
            }
            else
                return 1;
        }
    }
}

function MaximalSquare(strArr) {

    // initialize
    let submetrix = new Array(strArr.length);
    for (let i = 0; i < strArr.length; i++)
        submetrix[i] = new Array(strArr[i].length);

    for (let i = 0; i < strArr.length; i++) {
        for (let j = 0; j < strArr[i].length; j++) {
            submetrix[i][j] = strArr[i].charAt(j);
        }
    }

    // start algorithm
    let biggerSquare = 0;
    for (let i = 0; i < submetrix.length; i++) {
        for (let j = 0; j < submetrix[i].length; j++) {
            if (submetrix[i][j] === "1") {
                let attempt = FindSubMetrix(submetrix, i, j);
                if (attempt > biggerSquare)
                    biggerSquare = attempt;
            }
        }
    }

    // square submetrix: witdh === heigth, and that contains all 1's
    return biggerSquare;
}

console.log(MaximalSquare(["0111", "1111", "1111", "1111"]));
// 9

console.log(MaximalSquare(["0111", "1101", "0111"]));
// 1