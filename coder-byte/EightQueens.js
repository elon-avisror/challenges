/**
 * Have the function EightQueens(strArr) read strArr which will be an array consisting of the locations
 * of eight Queens on a standard 8x8 chess board with no other pieces on the board.
 * The structure of strArr will be the following: ["(x,y)", "(x,y)", ...]
 * where (x,y) represents the position of the current queen on the chessboard
 * (x and y will both range from 1 to 8 where 1,1 is the bottom-left of the chessboard and 8,8 is the top-right).
 * Your program should determine if all of the queens are placed in such a way where none of them
 * are attacking each other.
 * If this is true for the given input, return the string true
 * otherwise return the first queen in the list that is attacking another piece in the same format it was provided.
 * For example: if strArr is ["(2,1)", "(4,2)", "(6,3)", "(8,4)", "(3,5)", "(1,6)", "(7,7)", "(5,8)"]
 * then your program should return the string true.
 */

const maxWidth = 8;
const maxHeight = 8;
const minWidth = 1;
const minHeight = 1;

function right(h, w, o_h, o_w) {
    while (w < maxWidth) {
        w++;
        if (w === o_w && h === o_h)
            return false;
    }
    return true;
}

function left(h, w, o_h, o_w) {
    while (w > minWidth) {
        w--;
        if (w === o_w && h === o_h)
            return false;
    }
    return true;
}

function top(h, w, o_h, o_w) {
    while (h < maxHeight) {
        h++;
        if (w === o_w && h === o_h)
            return false;
    }
    return true;
}

function buttom(h, w, o_h, o_w) {
    while (h > minHeight) {
        h--;
        if (w === o_w && h === o_h)
            return false;
    }
    return true;
}

function rightTop(h, w, o_h, o_w) {

    while (w < maxWidth && h < maxHeight) {
        w++;
        h++;
        if (w === o_w && h === o_h)
            return false;
    }
    return true;
}

function leftTop(h, w, o_h, o_w) {

    while (w > minWidth && h < maxHeight) {
        w--;
        h++;
        if (w === o_w && h === o_h)
            return false;
    }
    return true;
}

function rightButtom(h, w, o_h, o_w) {

    while (w < maxWidth && h > minHeight) {
        w++;
        h--;
        if (w === o_w && h === o_h)
            return false;
    }
    return true;
}

function leftButtom(h, w, o_h, o_w) {

    while (w > minWidth && h < minHeight) {
        w--;
        h--;
        if (w === o_w && h === o_h)
            return false;
    }
    return true;
}

function EightQueens(strArr) {

    for (let i = 0; i < strArr.length; i++) {

        // check if one of other queens are in the locations that was found
        for (let j = 0; j < strArr.length; j++) {

            // another queens
            if (i !== j) {

                // this queen
                let thisQueen = strArr[i];
                let h = Number(thisQueen.charAt(1));
                let w = Number(thisQueen.charAt(3));

                // other queen
                let otherQueen = strArr[j];
                let o_h = Number(otherQueen.charAt(1));
                let o_w = Number(otherQueen.charAt(3));

                if (right(h, w, o_h, o_w) &&
                    left(h, w, o_h, o_w) &&
                    top(h, w, o_h, o_w) &&
                    buttom(h, w, o_h, o_w) &&
                    rightTop(h, w, o_h, o_w) &&
                    leftTop(h, w, o_h, o_w) &&
                    rightButtom(h, w, o_h, o_w) &&
                    leftButtom(h, w, o_h, o_w))
                    continue;
                else
                    return thisQueen;
            }
        }
    }

    return true;
}

console.log(EightQueens(["(2,1)", "(4,3)", "(6,3)", "(8,4)", "(3,4)", "(1,6)", "(7,7)", "(5,8)"]));
// "(2,1)"

console.log(EightQueens(["(2,1)", "(5,3)", "(6,3)", "(8,4)", "(3,4)", "(1,8)", "(7,7)", "(5,8)"]));
// "(5,3)"