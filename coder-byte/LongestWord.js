/**
 * Have the function LongestWord(sen) take the sen parameter being passed and return the largest word in the string.
 * If there are two or more words that are the same length, return the first word from the string with that length.
 * Ignore punctuation and assume sen will not be empty.
 */

function LongestWord(sen) {

    sen = sen.replace(/[^a-zA-Z0-9 ]/g, "");
    let res = sen.split(" ");
    let biggest = "";
    for (let i = 0; i < res.length; i++) {
        if (res[i].length > biggest.length)
            biggest = res[i];
    }
    return biggest;

}

console.log(LongestWord("fun&!! time"));
// time

console.log(LongestWord("I love dogs"));
// love