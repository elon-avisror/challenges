#!/bin/bash

# UNIX
# sed -i -e 's/\r$//' encoding.sh

#for letter in "${!letters[@]}"; do echo "$letter - ${letters[$letter]}"; done

function smorse()
{
    declare -A res=()
    for (( i=0; i<${#1}; i++ )); do
        res+="${letters[${1:$i:1}]}"
    done

    echo "$1 => ${res}"
}

function unique()
{
    declare -A arr=()
    declare -A ans=()
    # 1. The syntax is as follows for bash, ksh, zsh, and all other shells to read a file line by line:
    #    'while read -r line; do COMMAND; done < input.file'
    # 2. The '-r' option passed to read command prevents backslash escapes from being interpreted.
    # 3. Add 'IFS=' option before read command to prevent leading/trailing whitespace from being trimmed:
    #    'while IFS= read -r line; do COMMAND_on $line; done < input.file'
    input="$1"
    while IFS= read -r line
    do
        declare -A encode=()
        for (( i=0; i<${#line}; i++ )); do
            encode+="${letters[${line:$i:1}]}"
        done
        arr["$line"]="$encode"
        
        if [[ ans["${arr[$line]}"] -eq "" ]]; then
            ans["${arr[$line]}"]=1
        else
            ans["${arr[$line]}"]=$((ans["${arr[$line]}"]+1))
        fi

        if [[ "$encode" = *"---------------"* ]]; then
            echo "the only word with 15 dashes (-) is: $line"
        fi

    done < "$input"

    for encode in ${!ans[@]}; do
        if [[ ${ans[$encode]} -eq 13 ]]; then
            echo "the result is:"
            for word in ${!arr[@]}; do
                if [[ ${arr[$word]} == $encode ]]; then
                    echo "$word"
                fi
            done
        fi
    done
}

declare -A letters=(   
            ["a"]=".-" 
            ["b"]="-..."
            ["c"]="-.-." 
            ["d"]="-.." 
            ["e"]="." 
            ["f"]="..-." 
            ["g"]="--." 
            ["h"]="...." 
            ["i"]=".." 
            ["j"]=".---" 
            ["k"]="-.-" 
            ["l"]=".-.." 
            ["m"]="--" 
            ["n"]="-." 
            ["o"]="---" 
            ["p"]=".--." 
            ["q"]="--.-" 
            ["r"]=".-." 
            ["s"]="..." 
            ["t"]="-" 
            ["u"]="..-" 
            ["v"]="...-" 
            ["w"]=".--" 
            ["x"]="-..-" 
            ["y"]="-.--" 
            ["z"]="--.." )

printf "\nCHALLANGE:\n"
smorse "sos"
smorse "daily"
smorse "programmer"
smorse "bits"
smorse "three"

printf "\nBONUS example:\n"
smorse "needing"
smorse "nervate"
smorse "niding"
smorse "tiling"
smorse "autotomous"

printf "\nBONUS task:\n"
unique "$1"

# TESTING
#printf "\narr:\n"
#for word in "${!arr[@]}"; do echo "${arr[$word]} - $word"; done
#printf "\nans:\n"
#for encode in "${!ans[@]}"; do echo "$encode - ${ans[$encode]}"; done