#!/bin/bash

read nums

sum=0

for (( i=0; i<nums; i++ )); do
    read num
    sum="$(( sum + num ))"
done

printf "%.3f" $(echo "scale=4; $sum/$nums" | bc)
