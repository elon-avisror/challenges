/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function(nums) {
    let i = 0;
    while (i < nums.length) {
        let cnt = 0;
        for (let j = i; j+1 < nums.length; j++) {
            if (nums[j] === nums[j+1])
                cnt++;
            else
                break;
        }
        nums.splice(i, cnt);
        i++;
    }
    return nums.length;
};
