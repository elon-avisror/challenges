/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    return getMatch(nums, nums, target);
};

var getMatch = function(nums, tmp, target) {
    for (let i = 0; i < nums.length; i++) {
        for (let j = i+1; j < tmp.length; j++) {
            if (nums[i] + tmp[j] === target)
                return [i, j];
        }
    }
}
