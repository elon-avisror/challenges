/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function(s) {
    const stack = [];
    s = s.split('');
    for (let i = 0; i < s.length; i++) {
        switch (s[i]) {

            // opens
            case '(':
            case '{':
            case '[':
                stack.push(s[i]);
                break;

            // closes
            case ')':
                if (stack.pop() !== '(')
                    return false;
                break;
            case '}':
                if (stack.pop() !== '{')
                    return false;
                break;
            case ']':
                if (stack.pop() !== '[')
                    return false;
                break;
        }
    }
    return stack.length === 0;
};
