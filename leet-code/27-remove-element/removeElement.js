/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
var removeElement = function(nums, val) {
    let i = 0;
    while (i < nums.length) {
        let cnt = 0;
        for (let j = i; j < nums.length; j++) {
            if (nums[j] === val)
                cnt++;
            else
                break;
        }
        nums.splice(i, cnt);
        i++;
    }
    return nums.length;
};
