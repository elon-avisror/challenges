/**
 * @param {number} x
 * @return {number}
 */
var reverse = function(x) {
    const reverseNum = parseInt(x.toString().split('').reverse().join('')) * Math.sign(x);
    return (Math.abs(reverseNum) <= Math.pow(2, 31)) ? reverseNum : 0;
};
