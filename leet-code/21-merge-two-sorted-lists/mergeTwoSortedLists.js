/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var mergeTwoLists = function(l1, l2) {
    const l = new ListNode();
    let prev = l;
    while (l1 !== null || l2 !== null) {
        if (l1 && l2 && l1.val <= l2.val) {
            prev.next = l1;
            l1 = l1.next;
        }
        else if (l2) {
            prev.next = l2;
            l2 = l2.next;
        }
        else
            break;
        prev = prev.next;
    }

    if (l1 === null)
        prev.next = l2;
    if (l2 === null)
        prev.next = l1;

    return l.next;
};
