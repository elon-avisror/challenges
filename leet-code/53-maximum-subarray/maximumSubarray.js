/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function(nums) {
    let maxSoFar = Number.NEGATIVE_INFINITY;
	for (let i = 0; i < nums.length; i++) {
		let maxSum = 0;
		for (let j = i; j < nums.length; j++) {
			maxSum += nums[j];
			if (maxSoFar < maxSum)
				maxSoFar = maxSum;
		}
	}
	return maxSoFar;
};
