/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function(strs) {

    const smallest = strs.reduce((min, str) => min < str ? min : str, strs[0] || '');
    const largest  = strs.reduce((min, str) => min > str ? min : str, strs[0] || '');

    if (smallest) {
        for (let i = 0; i < smallest.length; i++) {
            if (smallest[i] != largest[i])
                return smallest.substr(0, i);
        }
    }
    else if (smallest > largest)
        return largest;

    return smallest || '';
};
