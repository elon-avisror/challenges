/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLastWord = function(s) {
    const sWords = s.split(' ');
    let lastLength = 0;
    for (let i = 0; i < sWords.length; i++) {
        if (sWords[i].length)
            lastLength = sWords[i].length;
    }
    return lastLength;
};