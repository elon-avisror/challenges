package Exam.MoedA.Exercise1;

public class Exercise1 {
    public int minDeadEndSnake(int[][] mat, int i, int j) {

        // right
        if (Math.abs(mat[i][j]-mat[i][j+1]) == 1) {
            System.out.println("right");
            return 1 + minDeadEndSnake(mat, i, j+1);
        }

        // down
        else if (Math.abs(mat[i][j]-mat[i+1][j]) == 1) {
            System.out.println("down");
            return 1 + minDeadEndSnake(mat, i+1, j);
        }

        return 1;
    }
}
