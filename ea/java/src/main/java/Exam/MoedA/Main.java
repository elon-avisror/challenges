package Exam.MoedA;

import Exam.MoedA.Exercise1.Exercise1;

public class Main {

    public static void main(String[] args) {

        // Exercise 1
        Exercise1 exercise1 = new Exercise1();
        int[][] mat = {
                {4,7,2,3,1},
                {3,4,1,4,4},
                {1,5,6,6,8},
                {3,4,5,8,9},
                {3,2,2,7,6}
        };
        System.out.println("Minimum dead-end snake path challenge:");
        System.out.println("The result is: " + exercise1.minDeadEndSnake(mat, 0, 0));
    }
}
