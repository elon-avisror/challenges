package Exam.MoedB.Exercise4;

public class One {
    protected int _a;
    public One()   {
        _a=1;
    }
    public One(int a) {
        _a=a;
    }
    public int getA(){
        return _a;
    }
    public int f(){
        return _a;
    }
}