package Exam.MoedB.Exercise4;

public class Exercise4 {
    public static void whichClass(Object obj) {
        // a == _a
        int a = ((One) obj).getA();
        // f == _a+1
        int f = ((One) obj).f();
        int g;
        if (a == f)
            System.out.println("One");
        if (a+1 == f)
            System.out.println("Two");
        if (a-1 == f)
            System.out.println("Four");
        if (a+2 == f) {
            // _a+2
            g = ((Three) obj).g();
            if (g+1 == f)
                System.out.println("Five");
            else
                System.out.println("Three");
        }
    }
}
