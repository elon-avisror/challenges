package Exam.MoedB.Exercise4;

public class Four extends Three {
    public Four(){
        super();
        _a++;
    }
    public Four(int a){
        super(a);
    }
    public Four(int a,int b){
        super();
        _a=_a+a+b;
    }
    public int f(){
        return _a-1;
    }
}