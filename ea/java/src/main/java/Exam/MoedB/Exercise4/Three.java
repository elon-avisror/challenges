package Exam.MoedB.Exercise4;

public class Three extends Two {
    public Three() {
        super();
    }

    public Three(int a) {
        super(a);
    }

    public int f() {
        return _a+2;
    }

    public int g() {
        return _a;
    }
}