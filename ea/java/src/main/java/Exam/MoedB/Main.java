package Exam.MoedB;

import Exam.MoedB.Exercise1.Exercise1;
import Exam.MoedB.Exercise2.Exercise2;
import Exam.MoedB.Exercise3.Exercise3;
import Exam.MoedB.Exercise4.*;
import Exam.MoedB.Exercise5.Exercise5;

public class Main {
    public static void main(String[] args) {

        // Exercise 1
        Exercise1 exercise1 = new Exercise1();
        char[][] arr = new char[][] {
                {'t', 'z', 'x', 'c', 'd'},
                {'s', 'h', 'a', 'z', 'x'},
                {'h', 'w', 'l', 'o', 'm'},
                {'o', 'r', 'n', 't', 'n'},
                {'a', 'b', 'r', 'i', 'n'}
        };
        String word = "shalom";
        //exercise1.findWord(arr, word);
        // Output:
        // [0, 0, 0, 0, 0]
        // [1, 2, 3, 0, 0]
        // [0, 0, 4, 5, 6]
        // [0, 0, 0, 0, 0]
        // [0, 0, 0, 0, 0]

        // Exercise 2
        // n = 8509
        Exercise2 exercise2 = new Exercise2();
        exercise2.findFactors(8509);
        // Output:
        // p = 67
        // q = 127

        // Exercise 3
        Exercise3 exercise3 = new Exercise3();
        // Follow coding in comments

        // Exercise 4
        Exercise4 exercise4 = new Exercise4();
        Exercise4.whichClass(new One());
        Exercise4.whichClass(new Two());
        Exercise4.whichClass(new Three());
        Exercise4.whichClass(new Four());
        Exercise4.whichClass(new Five());
        // Output:
        // One
        // Two
        // Three
        // Four
        // Five

        // Exercise 5
        Exercise5 exercise5 = new Exercise5();
        // Follow coding in comments
    }
}
