package Exam.MoedB.Exercise1;

public class Exercise1 {
    public void findWord(char[][] arr, String word) {
        int[][] matrix = new int[arr.length][arr.length];
        int k = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] == word.charAt(k)) {
                    matrix[i][j] = k+1;
                    k++;
                }
            }
        }
    }
}
