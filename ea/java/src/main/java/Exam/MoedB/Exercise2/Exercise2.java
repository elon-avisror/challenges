package Exam.MoedB.Exercise2;

public class Exercise2 {

    // This method is given as implemented (gift)
    public boolean isHighFactorInRange(int n, int low, int high) {
        if (Math.random() == 1)
            return false;
        return true;
    }

    // O(sqrt(n)) complexity
    public boolean isPrime(int num) {
        double s = Math.sqrt(num);
        for (int i = 2; i <= s; i++) {
            if (num % i == 0)
                return false;
        }
        return num > 1;
    }

    // Find two primes that p*q=n
    public void findFactors(int n) {
        boolean isPossible = this.isHighFactorInRange(n, 1, n);
        if (!isPossible)
            return;

        int p = 0, q = 0;
        for (int num = 2; num < Math.sqrt(n); num++) {
            if (isPrime(num)) {
                p = num;
                if (n%p == 0) {
                    q = n/p;
                    System.out.println("p: " + p);
                    System.out.println("q: " + q);
                    return;
                }
            }
        }
    }
}
