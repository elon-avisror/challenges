package Exam.MoedB.Exercise3;

public class Exercise3 {
    // Exercise 3
    // f method - returns the rightest son
    // g method - returns the first father with a right son

    // what method:
    // 1. if null return null
    // 2. if left son --> f (left son) [get the rightest son of the left son of node]
    // 3. else --> do g

    // secret method:
    // 1. null == true --> returns true
    // 2. do what with node --> returns node1
    // 2.1. if node1 == null --> returns true
    // 2.2. do what with node1 --> returns node2
    // 2.3. if node2 == null --> returns true
    // 3. if node !== node1 !== node2 --> returns false
    // 4. else, recursive secret on node1

    // something method:
    // return (secret(f(node)) --> on the rightest son of node, do secret

    // Output:

    // RUN#1
    // root = 160
    // temp1 = 99
    // temp2 = 61
    // return secret(99)

    // RUN#2
    // root = 99
    // temp1 = 61
    // temp2 = 38
    // return secret(61)

    // RUN#3
    // root = 61
    // temp1 = 38
    // temp2 = 23
    // return secret(38)

    // RUN#4
    // root = 38
    // temp1 = 23
    // temp2 = 15
    // return secret(23)

    // RUN#5
    // root = 23
    // temp1 = 15
    // temp2 = 7 (8)
    // OUTPUT: 23 != 15 + 7
    // return false!!!

    // RUN#6
    // root = 15
    // temp1 = 8
    // temp2 = null
    // returns true!!!
}
