package Exam.MoedB.Exercise5;

// what(a, 0, 9)
public class Exercise5 {
    // RUN#0
    // a = array of integers i.e. [1,2,3,4,...]
    // x = is the index
    // y = is the up limit
    // t = 7
    // p = 6
    // i = 10
    // a: [7, 5, 8, 2, 12, 1, 2, 6, 9, 3]
    // q: head [3, 6, 2, 1, 2, 5, 7, 8, 12, 9] tail

    // RUN#1
    // a: [7, 5, 8, 2, 12, 1, 2, 6, 9, 3]
    // x = 0
    // y = 9
    // q: head [] tail
    // p = 5

    // RUN#2
    // x = 0

    // t = 3
    // i = 0
    // y = 4
    // a: [3, 6, 2, 1, 2, 5, 7, 8, 12, 9]
    // q: head [2, 1, 2, 3, 6] tail
    // p = 3

    // RUN#2
    // i = 2
    // y = 2
    // t = 2
    // q: head [1, 2, 2] tail
    // a: [2, 1, 2, 3, 6, 5, 7, 8, 12, 9]
    // p = 1

    // RUN#3
    // y = 0
    // x = 0
    // p = 0
    // a: [1, 2, 2, 3, 6, 5, 7, 8, 12, 9]
    public static int what(int [] a, int x, int y) {
        int t = a[x];
        IsraeliQueue q = new IsraeliQueue();
        q.insertAtEnd(t);
        int p=x;
        for (int i = x+1; i <= y; i++) {
            if (a[i]<t) {
                q.insertAtHead(a[i]);
                p++;
            }
            else
                q.insertAtEnd(a[i]);
        }
        for (int i = x; i <= y; i++)
            a[i] = q.removeFromHead();
        return p;
    }

    // a: [7, 5, 8, 2, 12, 1, 2, 6, 9, 3]
    // p = 10
    public static void something(int [] a) {
        int p = a.length;
        while (p!=0)
            p = what(a, 0, p-1);
    }
}
