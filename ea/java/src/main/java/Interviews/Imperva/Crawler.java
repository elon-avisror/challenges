package Interviews.Imperva;

import java.io.File;
import java.util.List;

public class Crawler implements ActionAble {
    public List<File> getFiles(File f) {
        return null;
    }

    public boolean isDir(File f) { return false; }

    public void action(File f) {
        // TBD
    }

    public void crawl(File f, ActionAble actionAble) {
        if (!isDir(f)) {
            // interface: method action(EnumAction type)
            actionAble.action(f);
        }
        List<File> files = this.getFiles(f);
        for (File file : files) {
            this.crawl(file, actionAble);
        }
    }

    /*
    OUTPUT:
    a
    a1
    x
    */

    /*
    Example of the File-System:
    X
        A
            a
            A1
                a1
        x
     */
}
