package Interviews.Imperva;

// Anti-Pattern
public class Singleton {
    public static Singleton exercise;

    private Singleton() {}

    public synchronized static Singleton getSingleton() {
        if (exercise == null) {
            exercise = new Singleton();
        }
        return exercise;
    }
}
