package Interviews.Imperva;

import java.io.File;

public interface ActionAble {
    void action(File f);
}
