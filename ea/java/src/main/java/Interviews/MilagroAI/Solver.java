package Interviews.MilagroAI;

import java.util.Arrays;
import java.util.List;

public abstract class Solver implements Cracker {
    protected int sum;
    protected int start;
    protected int end;
    protected int length;
    protected int longest;

    private void inputs(int[] nums, int k) {
        System.out.println("The inputs are:");
        System.out.println("nums: " + Arrays.toString(nums));
        System.out.println("k:" + k);
    }

    private void outputs(List<Integer> res) {
        System.out.println("The outputs are:");
        if (res.isEmpty())
            System.out.println("Not found, so: " + this.longest);
        else {
            System.out.println("The longest contiguous subarray where its sum is equal to k is: " + this.longest);
            System.out.println("And the subarray is: " + res);
        }
    }

    private void initialize() {
        this.sum = 0;
        this.start = 0;
        this.end = 0;
        this.length = 0;
        this.longest = 0;
    }

    public void resolve(int[] nums, int k) {
        // Show the inputs
        this.inputs(nums, k);

        // Handle the solution for by the different solvers
        List<Integer> res = this.sol(nums, k);

        // Show the results
        this.outputs(res);

        // For the next time
        this.initialize();
    }

    public abstract List<Integer> sol(int[] nums, int k);
}