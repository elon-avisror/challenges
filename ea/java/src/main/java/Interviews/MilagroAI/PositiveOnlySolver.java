package Interviews.MilagroAI;

import java.util.ArrayList;
import java.util.List;

public class PositiveOnlySolver extends Solver {
    // Answer: O(n)
    @Override
    public List<Integer> sol(int[] nums, int k) {
        // O(n)
        for (int i = 0; i < nums.length; i++) {
            this.length++;
            this.sum += nums[i];

            if (!(this.sum < k)) {
                if (this.sum == k && this.length > this.longest) {
                    this.end = i + 1;
                    this.start = this.end - this.length;
                    this.longest = this.length;
                }

                // Initialize
                this.sum = 0;
                this.length = 0;
            }
        }

        List<Integer> res = new ArrayList<>();
        // O(n)
        for (int i = this.start; i < this.end; i++)
            res.add(nums[i]);

        return res;
    }
}
