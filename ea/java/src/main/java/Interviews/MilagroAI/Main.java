package Interviews.MilagroAI;

public class Main {
    public static void main(String args[]) {
        /*
        Question:
        Given an array of integers "int[] a" and some integer "int k".
        Provide with a java method to find the length of the longest contiguous subarray where its sum is equal to k.
        If no subarray meets the requirement return 0.
        */

        // Inputs
        // int[] nums = {10, 1, 2, 5, 4 ,6, 7 ,8 ,9 ,3};
        int[] nums = {10, -1, 2, -5, 4, -6, 7, -8, 9, -3};
        int k = 11;

        // Solutions
        Solver positiveOnlySolver = new PositiveOnlySolver();
        Solver negativePositiveSolver = new NegativePositiveSolver();

        if (isPositivesOnly(nums))
            positiveOnlySolver.resolve(nums, k); // O(n)
        else
            negativePositiveSolver.resolve(nums, k); // O(n^2)
    }

    // O(n)
    public static boolean isPositivesOnly(int[] nums) {
        for (int num : nums) {
            if (num < 0)
                return false;
        }
        return true;
    }
}