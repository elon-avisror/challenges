package Interviews.MilagroAI;

import java.util.List;

public interface Cracker {
    List<Integer> sol(int[] nums, int k);
}
