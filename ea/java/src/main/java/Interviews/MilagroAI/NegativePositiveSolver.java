package Interviews.MilagroAI;

import java.util.ArrayList;
import java.util.List;

public class NegativePositiveSolver extends Solver {
    private boolean resolved = false;

    // O(n^2)
    private List<Integer> findBestResult(int[] nums, int k) {
        // O(n)
        for (int i = 0; i < nums.length; i++) {
            int j;
            this.length++;
            this.sum += nums[i];

            // O(n)
            for (j = i+1; j < nums.length; j++) {
                this.length++;
                this.sum += nums[j];
            }

            if (this.sum == k && this.length > this.longest) {
                this.resolved = true;
                this.end = j;
                this.start = this.end - this.length;
                this.longest = this.length;
            }

            // Initialize
            this.sum = 0;
            this.length = 0;
        }

        List<Integer> res = new ArrayList<>();
        // O(n)
        for (int i = this.start; i < this.end; i++)
            res.add(nums[i]);

        return res;
    }

    // Answer: O(n^3)
    @Override
    public List<Integer> sol(int[] nums, int k) {
        List<Integer> res = new ArrayList<>();
        // O(n)
        while (!this.resolved) {
            // O(n^2)
            res = this.findBestResult(nums, k);
            nums = removeLastNum(nums);
        }
        return res;
    }

    private int[] removeLastNum(int[] nums) {
        int[] newNums = new int[nums.length-1];
        for (int i = 0; i < newNums.length; i++) {
            newNums[i] = nums[i];
        }
        return newNums;
    }
}
