const express = require("express");
const bodyParser = require("body-parser");
const fetch = require("node-fetch");

const app = express();
const youtubeSearchUrl = "https://www.youtube.com/results?search_query=";

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
});

app.get("/video", (req, res) => {
    // curl / fetch by GET http request youtube search html page content

    // use ajax and parse the html file

    // res.send(youtubeSearchContent)

    console.log(youtubeSearchUrl + req.query.ytsearch);
    fetch(encodeURI(youtubeSearchUrl + req.query.ytsearch.toString()))
        .then((res) => res.text())
        .then((body) => res.send(body));
});

app.listen(3000);
