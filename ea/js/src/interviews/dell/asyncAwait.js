let c;

async function foo() {
  let x = await 5;
  b = 50;
  c = 5;
  console.log(b);
  console.log(c);
}

async function bar() {
  return 5;
}

foo().then(() => {
  console.log("after the promise");
});

console.log(foo());
console.log(bar());
console.log("a:", a);
console.log("b:", b);
console.log("c:", c);

var a = 20;
var b;
