function maxRounds(arr: number[], size: number, round: number): number {

    let max: number = 0;

    // rounds = O(k)
    for (let i: number = 1; i <= round; i++) {

        let i_max: number = 0;

        // array items = O(n)
        for (let j: number = 0; j < size; j++) {

            // biggest number
            if (i === 1 && i_max < arr[j]) {
                i_max = arr[j];
            }
            else {

                // round_2: all others < *100* < 314
                // roond_3: all others < *55* < 100
                // ...
                // round_i: all others < i_biggest < i-1_biggest
                if (i_max < arr[j] && arr[j] < max) {
                    i_max = arr[j];
                }
            }
        }

        // biggest i number
        max = i_max;

        console.log('round: ' + i);
        console.log('max: ' + max);
    }

    return max;
}

let arr: number[] = [7, 5, 8, 20, 100, 9, 22, 34, 55, 314, 4, 54];

console.log('the answer is: ' + maxRounds(arr, arr.length - 1, 5));