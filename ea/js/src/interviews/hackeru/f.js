// O(n)
function f(nums) {
    const result = [], dict = {}, loc = {};
    if (!Array.isArray(nums))
        return;
    nums.forEach((num, i) => {
        if (!dict[num]) {
            dict[num] = 1;
            loc[num] = i;
        }
        else
            dict[num] += 1;
    });
    nums.forEach((num, i) => {
        if (loc[num] === i) {
            result.push(num);
            result.push(dict[num]);
        }
    });
    return result;
}

const ans1 = f([1, 4, 2, 5, 3, 1, 6, 5, 4, 3]);
console.log('ans1:', ans1); // ans1: [1, 2, 4, 2, 2, 1, 5, 2, 3, 2, 6, 1]

const ans2 = f([1, 2, 3]);
console.log('ans2:', ans2); // ans2: [1, 1, 2, 1, 3, 1]

const ans3 = f([1, 4, 2, 3]);
console.log('ans3:', ans3); // ans3: [1, 1, 4, 1, 2, 1, 3, 1]