const emptyString = '';
const emptyObject = {};
const zeroNumber = 0;
const undefinedValue = undefined;
const nullValue = null;
const NaNValue = NaN;
const someString = 'Elon Avisror';

console.log('Do not do!!!');
console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
console.log(`(emptyString !== NaN) => ${emptyString !== NaN}`);
console.log(`(emptyObject !== NaN) => ${emptyObject !== NaN}`);
console.log(`(zeroNumber !== NaN) => ${zeroNumber !== NaN}`);
console.log(`(undefinedValue !== NaN) => ${undefinedValue !== NaN}`);
console.log(`(nullValue !== NaN) => ${nullValue !== NaN}`);
console.log(`(NaNValue !== NaN) => ${NaNValue !== NaN}`);
console.log(`(someString !== NaN) => ${someString !== NaN}`)
console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

console.log('');
console.log('Look for another strange isNaN function (Do not use it eather)');
console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
console.log(`(!isNaN(emptyString)) => ${!isNaN(emptyString)}`);
console.log(`(!isNaN(emptyObject)) => ${!isNaN(emptyObject)}`);
console.log(`(!isNaN(zeroNumber)) => ${!isNaN(zeroNumber)}`);
console.log(`(!isNaN(undefinedValue)) => ${!isNaN(undefinedValue)}`);
console.log(`(!isNaN(nullValue)) => ${!isNaN(nullValue)}`);
console.log(`(!isNaN(NaNValue)) => ${!isNaN(NaNValue)}`);
console.log(`(!isNaN(someString)) => ${!isNaN(someString)}`);
console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

console.log('');
console.log('Use instead Number.isNaN function!!!');
console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
console.log(`(!Number.isNaN(emptyString)) => ${!Number.isNaN(emptyString)}`);
console.log(`(!Number.isNaN(emptyObject)) => ${!Number.isNaN(emptyObject)}`);
console.log(`(!Number.isNaN(zeroNumber)) => ${!Number.isNaN(zeroNumber)}`);
console.log(`(!Number.isNaN(undefinedValue)) => ${!Number.isNaN(undefinedValue)}`);
console.log(`(!Number.isNaN(nullValue)) => ${!Number.isNaN(nullValue)}`);
console.log(`(!Number.isNaN(NaNValue)) => ${!Number.isNaN(NaNValue)}`);
console.log(`(!Number.isNaN(someString)) => ${!Number.isNaN(someString)}`);
console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');