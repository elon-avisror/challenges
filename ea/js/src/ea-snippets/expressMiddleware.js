const express = require('express');
const app = express();

// First middleware bedire response is
app.use((req, res, next) => {
    console.log('Start');
    next();
});

// Route handler
app.get('/', (req, res, next) => {
    res.send('Middle');
    next();
});

app.use('/', (req, res) => {
    console.log('End');
});

app.listen(3000);