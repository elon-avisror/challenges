const limiter = new Bottleneck({
  minTime: 5000,
  maxConcurrent: 1,
});
limiter.on("error", async function () {
  console.log("error", error);
});
limiter.on("done", async function () {
  limiter.schedule(() => doJob());
});

router.get("/worker/on", async (req, res) => {
  limiter.schedule(() => doJob());
  return res.status(200).send("on");
});

router.get("/worker/off", async (req, res) => {
  try {
    const running = await limiter.running();
    if (running) {
      await limiter.stop({ dropWaitingJobs: true });
    }
  } catch (error) {
    console.log("error", error);
  }
  return res.status(200).send("off");
});
