class Employee {
	constructor() {
		this.name = "Elon Avisror"
		this.age = 30
		this.city = "Yeruham"
		this.address = {
			street: "Hashmonaim",
			number: 16
		}
	}

	getClientObject() {
		this.city = "Tel-Aviv";
		// for flat objects
		// const clientObject = { ...this };

		// for nested objects (like this employee)
		const clientObject = JSON.parse(JSON.stringify(this));
		delete clientObject.city;
		clientObject.address.number = 32323;
		return clientObject;
	}
}

const employee = new Employee();
console.log(employee);

const cloneEmployee = employee.getClientObject();
console.log(employee);
console.log(cloneEmployee);
