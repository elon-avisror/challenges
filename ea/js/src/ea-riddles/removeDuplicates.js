/*
Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.
Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
*/

/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function(nums) {
    let i = 0;
    while (i < nums.length) {
        let cnt = 0;
        for (let j = i; j+1 < nums.length; j++) {
            if (nums[j] === nums[j+1])
                cnt++;
            else
                break;
        }
        nums.splice(i, cnt);
        i++;
    }
    return nums.length;
};
