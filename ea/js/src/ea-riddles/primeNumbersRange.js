// Task: find out how many prime numbers there are between 500 and 5000

// O(n) complexity - good!
// isPrime = num => {
//     for (let i = 2; i < num; i++) {
//         if (num % i === 0)
//             return false;
//     }
//     return num > 1;
// }

// O(sqrt(n)) complexity - better!
const isPrime = num => {
    for(let i = 2, s = Math.sqrt(num); i <= s; i++)
        if (num % i === 0)
            return false; 
    return num > 1;
}

getPrimes = (downLimit, upLimit) => {
    const results = [];
    for (let num = downLimit; num < upLimit; num++) {
        if (isPrime(num))
            results.push(num);
    }
    return results;
}


const downLimit = 500;
const upLimit = 5000;

console.log(`There are ${getPrimes(downLimit, upLimit).length} prime numbers between ${downLimit}-${upLimit}`);