class Class {
    private field = 'Elon';
};

const f = Function('field');

const c = new Class();
console.log(Object.getOwnPropertyNames(c));

console.log(Object.getOwnPropertyNames(f));