function checkGrid(grid) {
  for (let row = 0; row < grid.length; row++) {
    for (let col = 0; col < grid.length; col++) {
      if (
        grid[row][col] === "-" ||
        grid[row][col] === "|" ||
        grid[row][col] === "+"
      ) {
        return false;
      }
    }
  }
  return true;
}

function mark(grid, loc) {
  grid[loc.row][loc.col] = " ";
  return grid;
}

function vertical(loc) {
  loc.next = "vertical";
  if (loc.dir === "up") {
    console.log("up");
    loc.row = loc.row - 1;
  } else if (loc.dir === "down") {
    console.log("down");
    loc.row = loc.row + 1;
  } else {
    loc = null;
  }
  return loc;
}

function horizontal(loc) {
  loc.next = "horizontal";
  if (loc.dir === "right") {
    console.log("right");
    loc.col = loc.col + 1;
  } else if (loc.dir === "left") {
    console.log("left");
    loc.col = loc.col - 1;
  } else {
    loc = null;
  }
  return loc;
}

function findWay(grid, loc) {
  let right, up, left, down;
  const flag = {
    right: false,
    up: false,
    left: false,
    down: false,
  };
  if (grid[loc.row][loc.col + 1] !== undefined) {
    right = grid[loc.row][loc.col + 1];
    flag.right = true;
  }
  if (grid[loc.row - 1] !== undefined) {
    up = grid[loc.row - 1][loc.col];
    flag.up = true;
  }
  if (grid[loc.row][loc.col - 1] !== undefined) {
    left = grid[loc.row][loc.col - 1];
    flag.left = true;
  }
  if (grid[loc.row + 1] !== undefined) {
    down = grid[loc.row + 1][loc.col];
    flag.down = true;
  }

  if (
    flag.right &&
    (right === "-" || right === "+" || right === "X") &&
    loc.dir !== "left" &&
    (loc.next === "horizontal" || loc.next === "anywhere")
  ) {
    loc.col = loc.col + 1;
    loc.dir = "right";
    console.log("right");
  } else if (
    flag.up &&
    (up === "|" || up === "+" || up === "X") &&
    loc.dir !== "down" &&
    (loc.next === "vertical" || loc.next === "anywhere")
  ) {
    loc.row = loc.row - 1;
    loc.dir = "up";
    console.log("up");
  } else if (
    flag.left &&
    (left === "-" || left === "+" || left === "X") &&
    loc.dir !== "right" &&
    (loc.next === "horizontal" || loc.next === "anywhere")
  ) {
    loc.col = loc.col - 1;
    loc.dir = "left";
    console.log("left");
  } else if (
    flag.down &&
    (down === "|" || down === "+" || down === "X") &&
    loc.dir !== "up" &&
    (loc.next === "vertical" || loc.next === "anywhere")
  ) {
    loc.row = loc.row + 1;
    loc.dir = "down";
    console.log("down");
  } else {
    loc = null;
  }
  return loc;
}

function step(grid, loc) {
  switch (grid[loc.row][loc.col]) {
    case "X":
      grid = mark(grid, loc);
      loc = findWay(grid, loc);
      break;
    case "+":
      if (loc.dir === "right" || loc.dir === "left") {
        loc.next = "vertical";
      } else if (loc.dir === "up" || loc.dir === "down") {
        loc.next = "horizontal";
      }
      grid = mark(grid, loc);
      loc = findWay(grid, loc);
      break;
    case "-":
      grid = mark(grid, loc);
      loc = horizontal(loc);
      break;
    case "|":
      grid = mark(grid, loc);
      loc = vertical(loc);
      break;
    default:
      loc = null;
      break;
  }
  return loc;
}

function play(grid, loc) {
  let end = false,
    res = false;
  while (!end) {
    loc = step(grid, loc);

    if (!loc) {
      end = true;
      res = false;
    } else if (grid[loc.row][loc.col] === "X") {
      end = true;
      res = true;
      grid = mark(grid, loc);
    }
  }
  console.log("loc:", loc);
  return res;
}

function findX(grid) {
  for (let row = 0; row < grid.length; row++) {
    for (let col = 0; col < grid[row].length; col++) {
      if (grid[row][col] === "X") {
        return { row, col, dir: "center", next: "anywhere" };
      }
    }
  }
}

function line(grid) {
  let X = findX(grid);
  console.log("X:", X);
  const res = play(grid, X);
  console.log("loc:", res);
  console.log("grid:", grid);
  return res && checkGrid(grid);
}
