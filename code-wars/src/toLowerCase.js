function convertLowerCase(str) {
  return str.toLowerCase();
}

console.log(convertLowerCase("Hello world!"), "hello world!");
